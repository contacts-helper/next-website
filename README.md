# Contacts Helper WebSite

## Prerequisites

- [x] Docker
- [x] Docker Compose

## How to start

Run the docker container with `docker-compose up -d`.
Enter into the container (with: `docker container exec -ti contacts-helper-next bash`) and run `yarn` and after `yarn dev`.
Visit: `http://localhost` and you are ready to start.

## Contributors

- Lorenzo Calamandrei <nexcal.dev@gmail.com>
