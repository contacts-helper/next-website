import {
  faAppStoreIos,
  faGooglePlay
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { NextPage } from 'next';
import React from 'react';
import { Button, Jumbotron, OverlayTrigger, Tooltip } from 'react-bootstrap';
import Container from '../components/container/container';

const Index: NextPage = () => {
  const notAvailableTooltip = (id: string) => (
    <Tooltip id={`notAvailable-${id}`}>Not already available.</Tooltip>
  );

  return (
    <Container>
      <Jumbotron>
        <h1 className="text-center">Contacts Helper!</h1>
        <p className="text-center">
          Contacts Helper App - Mantain your contacts up to date!
        </p>
        <hr />
        <p className="d-flex justify-content-center">
          <OverlayTrigger
            placement="bottom"
            overlay={notAvailableTooltip('apple')}
          >
            <Button variant="dark" className="mr-4">
              <FontAwesomeIcon className="mr-2" icon={faAppStoreIos} />
              Apple Store
            </Button>
          </OverlayTrigger>
          <OverlayTrigger
            placement="bottom"
            overlay={notAvailableTooltip('gplay')}
          >
            <Button variant="warning">
              <FontAwesomeIcon icon={faGooglePlay} />
              Google Play
            </Button>
          </OverlayTrigger>
        </p>
      </Jumbotron>
    </Container>
  );
};

export default Index;
