import { NextPage } from 'next';
import React from 'react';
import { Col, Row } from 'react-bootstrap';
import Container from '../../components/container/container';

const policy: NextPage = () => {
  return (
    <Container>
      <h1>Contacts Helper - Privacy Policy</h1>
      <hr />
      <Row>
        <Col>
          <p>
            Actually there aren't a specific privacy polocy text, no data will
            be saved in other places except your phone.
          </p>
        </Col>
      </Row>
    </Container>
  );
};

export default policy;
