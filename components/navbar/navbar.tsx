import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import classes from './style.scss';

const navbar = () => {
  const router = useRouter();

  const getLinkClasses = (route: string) => {
    if (route === router.pathname) {
      return [classes.ActiveLink, 'mr-3'].join(' ');
    }
    return [classes.Link, 'mr-3'].join(' ');
  };

  return (
    <Navbar bg="dark" variant="dark" expand="md">
      <Navbar.Brand className="mr-3 font-weight-bold">
        Contacts Helper
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="navbar-menu" />
      <Navbar.Collapse id="navbar-menu">
        <Nav>
          <Nav.Link className={getLinkClasses('/')}>
            <Link href="/">
              <span className="text-white">Home</span>
            </Link>
          </Nav.Link>
          <Nav.Link className={getLinkClasses('/privacy/policy')}>
            <Link href="/privacy/policy">
              <span className="text-white">Privacy Policy</span>
            </Link>
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default navbar;
